'''
  Class is a blue print for creating objects
  
'''
	
class Person:
	"""
	   Constructor, is special method used for 
	   initializing instance variables.
	   Constructors are called automatically when object is 
	   created
	"""
	
	# Constructor
	def __init__(self,name,age,location):
		self.name = name
		self.age = age
		self.location = location
		
	def walk(self):
		print(self.name, 'walks')
	
	def eat(self):
		print(self.name, 'eats')
		

p = Person('Rahul',24,'Pune')

p.walk()
p.eat()
